Format: 3.0 (quilt)
Source: socnetv
Binary: socnetv
Architecture: any
Version: 2.8-1
Maintainer: Adrià García-Alzórriz <adria@fsfe.org>
Homepage: https://socnetv.org
Standards-Version: 4.5.1
Vcs-Browser: https://github.com/socnetv/app/tree/master/
Vcs-Git: https://github.com/socnetv/app.git
Build-Depends: debhelper-compat (= 13), libqt5charts5-dev, libqt5svg5-dev, qtbase5-dev, qtbase5-dev-tools
Package-List:
 socnetv deb math optional arch=any
Checksums-Sha1:
 b5f932cb90f9752d7de990f9da1b88473ef5d04c 858912 socnetv_2.8.orig.tar.gz
 605ff021ef1240398ac278dd8049e6ee13f752a7 18964 socnetv_2.8-1.debian.tar.xz
Checksums-Sha256:
 a8a89ecf7da0573b556b647445201531db81e600ef72954bd4a85dfb421ee19e 858912 socnetv_2.8.orig.tar.gz
 700fed8748da0411705adcff70d546dcb8d76320635234060e4b7c3e33b40628 18964 socnetv_2.8-1.debian.tar.xz
Files:
 69bfbdd86b98a9160a9ce8f9c641e863 858912 socnetv_2.8.orig.tar.gz
 95bb7a9514fb6c4c65059d20b2538294 18964 socnetv_2.8-1.debian.tar.xz
