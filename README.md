# My Sandbox

Not really much to see here, it's just a place where I push how
packages would look like if I'd maintain them.

This place is intended for self-learning about Debian packaging.

In case any package would become acceptable (basically in terms of
lintian) I'd merge eventually its branch to `master`.
